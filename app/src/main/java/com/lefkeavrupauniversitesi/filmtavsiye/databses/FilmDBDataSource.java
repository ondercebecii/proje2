package com.lefkeavrupauniversitesi.filmtavsiye.databses;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lefkeavrupauniversitesi.filmtavsiye.model.FilmObj;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FilmDBDataSource {

    SQLiteDatabase db;
    public FilmDBDataSource(SQLiteDatabase db) {
        this.db = db;
    }


    public int getAllFilmCount(){
        Cursor cursor = db.rawQuery("select * from filmler",null);

        return cursor.getCount();
    }

    public List<FilmObj> getAllFilms() {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }

    public List<FilmObj> getAllFilmsWithcategory(String kategori) {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler where kategori = '"+kategori+"'",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }

    public List<FilmObj> getRandomFilm() {

        Random rand = new Random();
        int n = rand.nextInt(getAllFilmCount()-1);


        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler limit "+n+",1",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }


    public List<String> getAllPlayers(){
        List<String> categories = new ArrayList<>();
        Cursor cursor = db.rawQuery("select oyuncu from filmler group by oyuncu order by oyuncu",null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            String k= cursor.getString(0);
            categories.add(k);
            cursor.moveToNext();
            //System.out.println("<item android:title=\""+k+"\"></item>");

        }
        return categories;

    }

    public List<String> getAllCategories(){
        List<String> categories = new ArrayList<>();
        Cursor cursor = db.rawQuery("select kategori from filmler group by kategori order by kategori",null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            String k= cursor.getString(0);
            categories.add(k);
            cursor.moveToNext();
            //System.out.println("<item android:title=\""+k+"\"></item>");

        }
        return categories;

    }

    public List<FilmObj> getAllFilmsWithImdb(Float score) {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler where imdb > '"+score+"'",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }

    public List<FilmObj> getAllFilmsWithPlayer(String player) {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler where oyuncu = '"+player+"'",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }


    public List<FilmObj> getAllFilmsWithIC(Float score,String kategori) {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler where kategori = '"+kategori+"' and imdb >"+score+" ",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }

    public List<FilmObj> getAllFilmsWithIP(Float score,String player) {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler where oyuncu = '"+player+"' and imdb >"+score+" ",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }

    public List<FilmObj> getAllFilmsWithCP(String kategori,String player) {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler where oyuncu = '"+player+"' and kategori = '"+kategori+"' ",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }



    public List<FilmObj> getAllFilmsWithICP(Float score,String kategori,String player) {

        List<FilmObj> films = new ArrayList<>();
        Cursor cursor =db.rawQuery("select * from filmler where oyuncu = '"+player+"' and kategori = '"+kategori+"' and imdb >"+score+" ",null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            int id = cursor.getInt(0);
            String filmName = cursor.getString(1);
            float imdb = cursor.getFloat(2);
            String category = cursor.getString(3);
            String playerName = cursor.getString(4);
            String trailerLink = cursor.getString(5);
            String info = cursor.getString(6);

            FilmObj obj = new FilmObj(id,filmName,imdb,category,playerName,trailerLink,info);
            films.add(obj);
            cursor.moveToNext();
        }

        return films;

    }





}
