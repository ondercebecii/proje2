package com.lefkeavrupauniversitesi.filmtavsiye.utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lefkeavrupauniversitesi.filmtavsiye.R;
import com.lefkeavrupauniversitesi.filmtavsiye.classes.YoutubePlayerActivity;
import com.lefkeavrupauniversitesi.filmtavsiye.fragments.RandomFragment;
import com.lefkeavrupauniversitesi.filmtavsiye.model.FilmObj;

import java.util.List;

public class FilmRecyclerViewArapter extends RecyclerView.Adapter<FilmRecyclerViewArapter.MyViewHolder> {

    LayoutInflater inflater;
    List<FilmObj> titleList;
    String mode;
    Context context;
    Fragment fragment;

    public FilmRecyclerViewArapter(Context context, List<FilmObj> titleList,String mode,Fragment fragment){
        inflater = LayoutInflater.from(context);
        this.titleList = titleList;
        this.mode = mode;
        this.context =context;
        this.fragment = fragment;
    }




    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.single_movie_item, viewGroup, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        myViewHolder.setData(titleList.get(position));
    }

    @Override
    public int getItemCount() {
        return titleList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView filmName;
        TextView imdb;
        TextView category;
        TextView playerName;
        TextView info;
        CardView card;
        View viewItem;

        public MyViewHolder(View itemView) {
            super(itemView);

            filmName = itemView.findViewById(R.id.tvFilmName);
            imdb = itemView.findViewById(R.id.tvImdb);
            category = itemView.findViewById(R.id.tvKategori);
            playerName = itemView.findViewById(R.id.tvPlayerName);
            info = itemView.findViewById(R.id.tvInfo);
            card = itemView.findViewById(R.id.card);

            viewItem = itemView;
        }

        public void setData(final FilmObj subject){
            filmName.setText(subject.getFilmName());
            Float puan = subject.getImdb();
            String s= puan.toString();
            imdb.setText(s);
            category.setText(subject.getCategory());
            playerName.setText(subject.getPlayerName());
            info.setText(subject.getInfo());

            card.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(mode.equals("random")){
                        ((RandomFragment)fragment).getRandomFilm(inflater);
                    }
                    return false;
                }
            });

            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, YoutubePlayerActivity.class);
                    intent.putExtra("link",subject.getTrailerLink());
                    context.startActivity(intent);
                }
            });





        }
    }

}
