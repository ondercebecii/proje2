package com.lefkeavrupauniversitesi.filmtavsiye.model;

public class FilmObj {

    private int id;
    private String filmName;
    private float imdb;
    private String category;
    private String playerName;
    private String trailerLink;
    private String info;


    public FilmObj(int id, String filmName, float imdb, String category, String playerName, String trailerLink, String info) {
        this.id = id;
        this.filmName = filmName;
        this.imdb = imdb;
        this.category = category;
        this.playerName = playerName;
        this.trailerLink = trailerLink;
        this.info = info;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public float getImdb() {
        return imdb;
    }

    public void setImdb(float imdb) {
        this.imdb = imdb;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getTrailerLink() {
        return trailerLink;
    }

    public void setTrailerLink(String trailerLink) {
        this.trailerLink = trailerLink;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}