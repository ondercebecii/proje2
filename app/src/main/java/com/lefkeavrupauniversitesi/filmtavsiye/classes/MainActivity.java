package com.lefkeavrupauniversitesi.filmtavsiye.classes;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lefkeavrupauniversitesi.filmtavsiye.R;
import com.lefkeavrupauniversitesi.filmtavsiye.fragments.AllMovieFragment;
import com.lefkeavrupauniversitesi.filmtavsiye.fragments.CategoryFragment;
import com.lefkeavrupauniversitesi.filmtavsiye.fragments.RandomFragment;
import com.lefkeavrupauniversitesi.filmtavsiye.fragments.SearchFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {



    ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        viewPager = findViewById(R.id.viewpager);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllMovieFragment(),"Anasayfa");
        adapter.addFragment(new CategoryFragment(),"Kategoriler");
        adapter.addFragment(new RandomFragment(),"Rastgele");
        adapter.addFragment(new SearchFragment(),"Arama");


        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }










    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }

}
