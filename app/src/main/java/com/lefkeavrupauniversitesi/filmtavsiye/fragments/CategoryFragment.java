package com.lefkeavrupauniversitesi.filmtavsiye.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import com.lefkeavrupauniversitesi.filmtavsiye.R;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBDataSource;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBHelper;
import com.lefkeavrupauniversitesi.filmtavsiye.model.FilmObj;
import com.lefkeavrupauniversitesi.filmtavsiye.utils.FilmRecyclerViewArapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {

    public CategoryFragment() {
        // Gerekli boş genel kurucu
    }

    LinearLayout spinnerLayout;
    EditText et;
    FilmDBDataSource dataSource;
    RecyclerView rvFilm;
    View view;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_category, container, false);
        spinnerLayout = view.findViewById(R.id.spinnerLayout);
        et = view.findViewById(R.id.editSpinner);

        spinnerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(),spinnerLayout);
                List<String> categories = dataSource.getAllCategories();
                popup.getMenu().add("");
                for(String s:categories){
                    popup.getMenu().add(s);
                }
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                            et.setText(item.getTitle());
                            if (et.getText().toString().equals("")){
                                initCategory(inflater);
                            }else{
                                setFilter(item.getTitle().toString(),inflater);
                            }

                            return false;
                    }
                });

                popup.show();
            }
        });

        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerLayout.performClick();
            }
        });



        initCategory(inflater);



        return view;
    }

    private void setFilter(String title,LayoutInflater inflater) {

        rvFilm = view.findViewById(R.id.rvFilm);
        dataSource = new FilmDBDataSource(new FilmDBHelper(inflater.getContext()).openDatabase());
        List<FilmObj> films = dataSource.getAllFilmsWithcategory(title);
        dataSource.getAllCategories();

        rvFilm.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        FilmRecyclerViewArapter adapter = new FilmRecyclerViewArapter(inflater.getContext(),films,"",this);
        rvFilm.setAdapter(adapter);


    }

    private void initCategory(LayoutInflater inflater) {
        rvFilm = view.findViewById(R.id.rvFilm);
        dataSource = new FilmDBDataSource(new FilmDBHelper(inflater.getContext()).openDatabase());
        List<FilmObj> films = dataSource.getAllFilms();
        dataSource.getAllCategories();

        rvFilm.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        FilmRecyclerViewArapter adapter = new FilmRecyclerViewArapter(inflater.getContext(),films,"",this);
        rvFilm.setAdapter(adapter);

    }
}
