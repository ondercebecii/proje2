package com.lefkeavrupauniversitesi.filmtavsiye.fragments;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.lefkeavrupauniversitesi.filmtavsiye.R;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBDataSource;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBHelper;
import com.lefkeavrupauniversitesi.filmtavsiye.model.FilmObj;
import com.lefkeavrupauniversitesi.filmtavsiye.utils.FilmRecyclerViewArapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RandomFragment extends Fragment {

    public RandomFragment() {
        //Gerekli boş genel kurucu
    }

    View view;
    FilmDBDataSource dataSource;
    RecyclerView rvFilm;
    ConstraintLayout mainLayout;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Bu parçanın düzenini şişir
        view =inflater.inflate(R.layout.fragment_random, container, false);



        rvFilm = view.findViewById(R.id.rvFilm);
        mainLayout = view.findViewById(R.id.mainLayout);
        getRandomFilm(inflater);


        return view;
    }

        public void getRandomFilm(LayoutInflater inflater) {
            YoYo.with(Techniques.Tada)
                    .duration(700)
                    .repeat(1)
                    .playOn(rvFilm);

        dataSource = new FilmDBDataSource(new FilmDBHelper(inflater.getContext()).openDatabase());
        List<FilmObj> films = dataSource.getRandomFilm();
        dataSource.getAllCategories();

        rvFilm.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        FilmRecyclerViewArapter adapter = new FilmRecyclerViewArapter(inflater.getContext(),films,"random",this);
        rvFilm.setAdapter(adapter);
    }
}
