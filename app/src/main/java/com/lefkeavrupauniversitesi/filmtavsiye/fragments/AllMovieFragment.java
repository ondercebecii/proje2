package com.lefkeavrupauniversitesi.filmtavsiye.fragments;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lefkeavrupauniversitesi.filmtavsiye.R;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBDataSource;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBHelper;
import com.lefkeavrupauniversitesi.filmtavsiye.model.FilmObj;
import com.lefkeavrupauniversitesi.filmtavsiye.utils.FilmRecyclerViewArapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllMovieFragment extends Fragment {

    public AllMovieFragment() {
        //Gerekli boş genel kurucu
    }

    FilmDBDataSource dataSource;
    RecyclerView rvFilm;
    List<FilmObj> films;
    List<FilmObj> qFilms;
    View view;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_all_movie, container, false);

        qFilms = new ArrayList<>();
        SearchView search = view.findViewById(R.id.searchView);
        search.setLayoutParams(new Toolbar.LayoutParams(Gravity.RIGHT));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length()==0){
                    initFilm(inflater);
                }
                filterFilm(s,inflater);
                return false;
            }
        });

        initFilm(inflater);

        return view;
    }

    void initFilm(LayoutInflater inflater){
        rvFilm = view.findViewById(R.id.rvFilm);
        dataSource = new FilmDBDataSource(new FilmDBHelper(inflater.getContext()).openDatabase());
        films = dataSource.getAllFilms();
        dataSource.getAllCategories();

        rvFilm.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        FilmRecyclerViewArapter adapter = new FilmRecyclerViewArapter(inflater.getContext(),films,"",this);
        rvFilm.setAdapter(adapter);
    }

    private void filterFilm(String s,LayoutInflater inflater) {
        qFilms.clear();
        for (FilmObj f : films){
            if (f.getFilmName().toLowerCase().contains(s.toLowerCase())){
                qFilms.add(f);
            }
        }
        rvFilm.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        FilmRecyclerViewArapter adapter = new FilmRecyclerViewArapter(inflater.getContext(),qFilms,"",this);
        rvFilm.setAdapter(adapter);

    }
}
