package com.lefkeavrupauniversitesi.filmtavsiye.fragments;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import com.lefkeavrupauniversitesi.filmtavsiye.R;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBDataSource;
import com.lefkeavrupauniversitesi.filmtavsiye.databses.FilmDBHelper;
import com.lefkeavrupauniversitesi.filmtavsiye.model.FilmObj;
import com.lefkeavrupauniversitesi.filmtavsiye.utils.FilmRecyclerViewArapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    public SearchFragment() {
        // Required empty public constructor
    }

    View view;
    EditText etImdb;
    EditText etCategory;
    EditText etPlayer;
    LinearLayout categoryLayout;
    LinearLayout playerLayout;
    ImageView searchOpen;
    ConstraintLayout searchLayout;
    Button araButon;

    FilmDBDataSource dataSource;
    RecyclerView rvFilm;
    List<FilmObj> films;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);

        etImdb = view.findViewById(R.id.imdbEdit);
        etCategory = view.findViewById(R.id.categorySpinner);
        etPlayer = view.findViewById(R.id.playerSpinner);
        categoryLayout = view.findViewById(R.id.categoryLayout);
        playerLayout = view.findViewById(R.id.playerLayout);
        searchOpen = view.findViewById(R.id.searchOpen);
        searchLayout = view.findViewById(R.id.searchLayout);
        araButon = view.findViewById(R.id.araButon);
        rvFilm = view.findViewById(R.id.rvFilm);
        dataSource = new FilmDBDataSource(new FilmDBHelper(inflater.getContext()).openDatabase());


        etCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(),etCategory);
                List<String> categories = dataSource.getAllCategories();
                popup.getMenu().add("");
                for(String s:categories){
                    popup.getMenu().add(s);
                }
                //popup.inflate(R.menu.category_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        etCategory.setText(item.getTitle());
                        return false;
                    }
                });

                popup.show();
            }
        });

        categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etCategory.performClick();
            }
        });

        etPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(),etPlayer);
                List<String> players = dataSource.getAllPlayers();
                popup.getMenu().add("");
                for(String s:players){
                    popup.getMenu().add(s);
                }

                //popup.inflate(R.menu.player_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        etPlayer.setText(item.getTitle());

                        return false;
                    }
                });

                popup.show();
            }
        });

        playerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPlayer.performClick();
            }
        });


        searchOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchLayout.getVisibility() == View.VISIBLE){
                    searchLayout.setVisibility(View.GONE);
                }else{
                    searchLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        araButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search(inflater);
                searchLayout.setVisibility(View.GONE);
            }
        });

        initFilm(inflater);


        return view;
    }

    private void initFilm(LayoutInflater inflater) {


        films = dataSource.getAllFilms();
        rvFilm.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        FilmRecyclerViewArapter adapter = new FilmRecyclerViewArapter(inflater.getContext(),films,"",this);
        rvFilm.setAdapter(adapter);
    }

    private void search(LayoutInflater inflater) {


        films.clear();

        int value = 1;

        if (!etImdb.getText().toString().equals("")){
            value *=2;
        }
        if (!etCategory.getText().toString().equals("")){
            value *=3;
        }
        if (!etPlayer.getText().toString().equals("")){
            value *=4;
        }

        switch (value){
            case 1 :{//hepsi boş
                films = dataSource.getAllFilms();
                break;
            }

            case 2 :{//imdb sadece
                Float f= Float.parseFloat(etImdb.getText().toString());
                films = dataSource.getAllFilmsWithImdb(f);
                break;
            }

            case 3 :{//kategori sadece
                films = dataSource.getAllFilmsWithcategory(etCategory.getText().toString());
                break;
            }

            case 4 :{//oyuncu sadece
                films = dataSource.getAllFilmsWithPlayer(etPlayer.getText().toString());
                break;
            }

            case 6 :{//imdb kategori
                Float f= Float.parseFloat(etImdb.getText().toString());
                films = dataSource.getAllFilmsWithIC(f,etCategory.getText().toString());
                break;
            }

            case 8 :{//imdb oyuncu
                Float f= Float.parseFloat(etImdb.getText().toString());
                films = dataSource.getAllFilmsWithIP(f,etPlayer.getText().toString());
                break;
            }

            case 12 :{//kategori oyuncu
                films = dataSource.getAllFilmsWithCP(etCategory.getText().toString(),etPlayer.getText().toString());
                break;
            }

            case 24 :{//kategori oyuncu imdb
                Float f= Float.parseFloat(etImdb.getText().toString());
                films = dataSource.getAllFilmsWithICP(f,etCategory.getText().toString(),etPlayer.getText().toString());
                break;
            }
        }

        rvFilm.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        FilmRecyclerViewArapter adapter = new FilmRecyclerViewArapter(inflater.getContext(),films,"",this);
        rvFilm.setAdapter(adapter);




    }
}
